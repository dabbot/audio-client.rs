#![feature(async_await, await_macro, futures_api)]

#[macro_use] extern crate log;
#[macro_use] extern crate serde_derive;
#[macro_use] extern crate serde_json;

pub extern crate lavalink;

pub mod model;

mod client;
mod error;

pub use client::Client as AudioClient;
pub use error::{Error, Result};
