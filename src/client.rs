use crate::{
    error::Result,
    model::{AudioPlayerState, PlayerState},
};
use http_client_base::prelude::*;
use lavalink::{
    model::*,
    rest::Load,
};
use percent_encoding::{self, USERINFO_ENCODE_SET};
use reqwest::{
    r#async::Client as ReqwestClient,
    Url,
};
use std::{
    borrow::Cow,
    sync::Arc,
};

pub struct Client {
    host: String,
    inner: Arc<ReqwestClient>,
}

impl Client {
    pub fn new(client: Arc<ReqwestClient>, host: impl Into<String>) -> Self {
        Self {
            host: host.into(),
            inner: client,
        }
    }

    pub async fn current(&self, guild_id: u64) -> Result<PlayerState> {
        trace!("Getting player for guild {}", guild_id);

        let state = await!(self.player(guild_id))?;

        trace!("Current player state: {:?}", state);

        PlayerState::from_audio_player_state(state)
    }

    pub async fn equalizer(
        &self,
        guild_id: u64,
        bands: Vec<Band>
    ) -> Result<()> {
        debug!("Preparing equalizer: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/equalizer", None)?;
        let model = Equalizer::new(guild_id.to_string(), bands);
        await!(self.inner.post(url).json(&model).send().compat())?;

        Ok(())
    }

    pub async fn pause(&self, guild_id: u64) -> Result<()> {
        await!(self.raw_pause(guild_id, true))
    }

    pub async fn play<'a>(
        &'a self,
        guild_id: u64,
        track: impl Into<String> + 'a,
    ) -> Result<bool> {
        debug!("Preparing play: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/play", None)?;
        let model = Play::new(guild_id.to_string(), track.into(), None, None);
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        let mut resp = await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got play response: {}", guild_id);

        await!(resp.json().compat()).map_err(From::from)
    }

    pub async fn player(&self, guild_id: u64) -> Result<AudioPlayerState> {
        debug!("Preparing player: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/player", None)?;
        let model = json!({"guild_id": guild_id.to_string() });
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        let mut resp = await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got player response: {}", guild_id);

        await!(resp.json().compat()).map_err(From::from)
    }

    pub async fn raw_pause(&self, guild_id: u64, pause: bool) -> Result<()> {
        debug!("Preparing pause: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/pause", None)?;
        let model = Pause::new(guild_id.to_string(), pause);
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got pause response: {}", guild_id);

        Ok(())
    }

    pub async fn resume(&self, guild_id: u64) -> Result<()> {
        await!(self.raw_pause(guild_id, false))
    }

    pub async fn search<'a>(
        &'a self,
        identifier: impl AsRef<str> + 'a,
        percent_encode: bool,
    ) -> Result<Load> {
        let identifier = identifier.as_ref();

        debug!("Preparing search: {}", identifier);
        let encoded = if percent_encode {
            Cow::from(percent_encoding::utf8_percent_encode(
                identifier,
                USERINFO_ENCODE_SET,
            ))
        } else {
            Cow::from(identifier)
        };
        let url = format!(
            "http://{}/search?identifier={}",
            &self.host,
            encoded,
        );

        debug!("Parsing URL: {}", url);
        let url = Url::parse(&url)?;
        debug!("Parsed URL: {:?}", url);

        let mut resp = await!(self.inner.get(url).send().compat())?;
        debug!("Got search response: {}", identifier);

        await!(resp.json().compat()).map_err(From::from)
    }

    pub async fn seek(&self, guild_id: u64, position: i64) -> Result<()> {
        debug!("Preparing seek: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/seek", None)?;
        let model = Seek::new(guild_id.to_string(), position);
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got seek response: {}", guild_id);

        Ok(())
    }

    pub async fn skip(&self, guild_id: u64) -> Result<()> {
        debug!("Preparing skip: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/skip", None)?;
        let model = json!({ "guild_id": guild_id });
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got skip response: {}", guild_id);

        Ok(())
    }

    pub async fn stop(&self, guild_id: u64) -> Result<()> {
        debug!("Preparing stop: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/stop", None)?;
        let model = Stop::new(guild_id.to_string());
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got stop response: {}", guild_id);

        Ok(())
    }

    pub async fn voice_update(&self, update: VoiceUpdate) -> Result<()> {
        debug!("Preparing voice update");
        let url = http_client_base::url(
            false,
            &self.host,
            "/voice-update",
            None,
        )?;
        debug!("URL: {}", url);
        debug!("Model: {:?}", update);

        await!(self.inner.post(url).json(&update).send().compat())?;
        debug!("Got voice update response");

        Ok(())
    }

    pub async fn volume(&self, guild_id: u64, volume: u64) -> Result<()> {
        debug!("Preparing volume: {}", guild_id);
        let url = http_client_base::url(false, &self.host, "/volume", None)?;
        let model = Volume::new(guild_id.to_string(), volume as i32);
        debug!("URL: {}", url);
        debug!("Model: {:?}", model);

        await!(self.inner.post(url).json(&model).send().compat())?;
        debug!("Got volume response: {}", guild_id);

        Ok(())
    }

    pub async fn track_end(&self, event: EventTrackEnd) -> Result<()> {
        let url = http_client_base::url(false, &self.host, "/track-end", None)?;

        await!(self.inner.post(url).json(&event).send().compat())?;

        Ok(())
    }

    pub async fn track_exception(
        &self,
        event: EventTrackException,
    ) -> Result<()> {
        let url = http_client_base::url(
            false,
            &self.host,
            "/track-exception",
            None,
        )?;
        await!(self.inner.post(url).json(&event).send().compat())?;

        Ok(())
    }
}
