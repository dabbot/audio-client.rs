use crate::Result;
use humantime::FormattedDuration;
use lavalink::decoder::{self, DecodedTrack};
use std::{
    fmt::{Display, Formatter, Result as FmtResult},
    time::Duration,
};

/// A struct containing the state of a guild's audio player.
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct AudioPlayerState {
    /// The ID of the guild that the player represents.
    pub guild_id: u64,
    /// Whether the player is paused.
    pub paused: bool,
    /// The estimated position of the player.
    pub position: i64,
    /// The current time of the player.
    pub time: i64,
    /// The track that the player is playing.
    pub track: Option<String>,
    /// The volume setting, on a scale of 0 to 150.
    pub volume: i32,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct PlayerStateResponse {
    pub track: Option<String>,
    pub paused: bool,
    pub position: i64,
}

#[derive(Debug)]
pub struct PlayerState {
    pub guild_id: u64,
    pub paused: bool,
    pub position: i64,
    pub time: i64,
    pub track: Option<DecodedTrack>,
    pub track_raw: Option<String>,
    pub volume: i32,
}

impl PlayerState {
    pub fn is_playing(&self) -> bool {
        self.track.is_some()
    }
}

impl Display for PlayerState {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self.track.as_ref() {
            Some(track) => {
                let status = if self.paused {
                    "Paused"
                } else {
                    "Currently Playing"
                };

                write!(f,
                    "{}: **{}** by **{}** `[{}/{}]`\n{}",
                    status,
                    track.title,
                    track.author,
                    track_length_readable(self.position as u64),
                    track_length_readable(track.length),
                    track.url.as_ref().unwrap_or(&"(no url)".to_owned()),
                )
            },
            None => write!(f, "No song is currently playing.")
        }
    }
}

impl PlayerState {
    pub fn from_audio_player_state(state: AudioPlayerState) -> Result<Self> {
        let AudioPlayerState {
            guild_id,
            paused,
            position,
            time,
            track,
            volume,
        } = state;

        let decoded = match track {
            Some(ref bytes) => Some(decoder::decode_track_base64(bytes.clone())?),
            None => None,
        };

        Ok(Self {
            track: decoded,
            track_raw: track,
            guild_id,
            paused,
            position,
            time,
            volume,
        })
    }
}

fn track_length_readable(ms: u64) -> FormattedDuration {
    humantime::format_duration(Duration::from_secs(ms / 1000))
}

#[cfg(test)]
mod tests {
    use lavalink::decoder::DecodedTrack;
    use super::PlayerState;

    fn state_empty() -> PlayerState {
        PlayerState {
            guild_id: 1,
            paused: true,
            position: 6250,
            time: 0,
            track: None,
            volume: 100,
            track_raw: None,
        }
    }

    fn state_track() -> PlayerState {
        let mut state = state_empty();

        state.track = Some(DecodedTrack {
            author: "xKito Music".to_owned(),
            identifier: "zcn4-taGvlg".to_owned(),
            length: 184_000,
            source: "youtube".to_owned(),
            stream: false,
            title: "she - Prismatic".to_owned(),
            url: "https://www.youtube.com/watch?v=zcn4-taGvlg".to_owned().into(),
            version: 1,
        });

        state
    }

    #[test]
    fn test_is_playing() {
        assert!(!state_empty().is_playing());
        assert!(state_track().is_playing());
    }

    #[test]
    fn test_state_format() {
        assert_eq!(
            format!("{}", state_empty()),
            "No song is currently playing.",
        );

        assert_eq!(
            format!("{}", state_track()),
            "Paused: **she - Prismatic** by **xKito Music** `[6s/3m 4s]`
https://www.youtube.com/watch?v=zcn4-taGvlg");
    }
}
